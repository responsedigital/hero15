<!-- Start Hero 15 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A hero component with a backgorund color/image and large text. Optional scroll below icon -->
@endif
<div class="hero15"  is="fir-hero-15">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="hero15__wrap">
      <h1>
          {{ $title ?? 'This is my Hero 15 Pinecone' }}
      </h1>
      <svg class="hero15__down" data-scroller viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Hero" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Hero_4_1440" transform="translate(-135.000000, -696.000000)">
                <rect id="Rectangle" fill-rule="nonzero" x="0" y="0" width="1440" height="768"></rect>
                <polygon id="Path" fill="#0076FF" transform="translate(146.000000, 708.000000) rotate(90.000000) translate(-146.000000, -708.000000) " points="147.025 697 144.178 699.828 150.354 706.004 134 706.004 134 709.996 150.354 709.996 144.178 716.172 147.025 719 158 708"></polygon>
            </g>
        </g>
    </svg>
  </div>
</div>
<!-- End Hero 15 -->