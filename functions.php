<?php

namespace Fir\Pinecones\Hero15;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Hero15',
            'label' => 'Pinecone: Hero 15',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A hero component with a backgorund color/image and large text. Optional scroll below icon"
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID',
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'label' => 'Show Scroller Arrow?',
                            'name' => 'show_scroller',
                            'type' => 'true_false',
                            'message' => 'Yes, show the scroller arrow',
                            'default_value' => 1,
                            'ui' => 1   
                        ],
                        GlobalFields::getOptions(array(
                            'theme',
                        ))         
                    ]
                ]              
            ]
        ];
    }
}
