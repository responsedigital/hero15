class Hero15 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {
        //this.scroller = $('[data-scroller]', this) 
        this.scroller = this.querySelector('[data-scroller]');

        //this.parent = this.scroller.parentsUntil('.hero15')
        this.parent = this.scroller.closest('.hero15')
    }

    connectedCallback () {
        this.initHero15()
    }

    initHero15 () {
        const { options } = this.props
        const config = {

        }

        this.scroller.addEventListener('click', e => {
            const bottom = this.scroller.getBoundingClientRect().top + window.scrollY;
            window.scrollTo({
                top: bottom - 100,
                left: 0,
                behavior: 'smooth'
            })
        })

        // console.log("Init: Hero 15")
    }

}

window.customElements.define('fir-hero-15', Hero15, { extends: 'div' })
